FROM openjdk:8
WORKDIR /App
COPY ./cacaNiquel.jar .
CMD ["java","-jar","cacaNiquel.jar"]
